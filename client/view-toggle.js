Template.viewToggle.events({
    'click .today-view': function() {
        Session.set('viewing-list', 'today');
    },

    'click .maybe-view': function() {
        Session.set('viewing-list', 'maybe');
    }

});